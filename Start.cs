using Godot;
using System;

public class Start : Spatial
{

    public override void _Ready()
    {
        
        Global global = (Global) GetNode("/root/Global");
        
        // Debug
        if (false)
        {
           if(true){
               global.IsServer = true;
               GetTree().ChangeScene("res://Server.tscn");
           } else {
             GetTree().ChangeScene("res://Client.tscn");
           }
           return;
        } 

        string[] args = OS.GetCmdlineArgs();
        GD.Print(args.Length);

        if (args.Length == 0)
        {
            GD.Print("No --key=server argument found. Starting as Client");
            GetTree().ChangeScene("res://Client.tscn");
            return;
        }
        foreach (string arg in args)
        {

            if (arg.Contains("server"))
            {
                GD.Print("Start Server");
                global.IsServer = true;
                GetTree().ChangeScene("res://Server.tscn");
                return;
            }
            else if (arg.Contains("client"))
            {
                GD.Print("Start Client");
                GetTree().ChangeScene("res://Client.tscn");
                return;
            }
        }

    }


}
