using Godot;
using System;

public class Miryp : Spatial
{

    bool isServer = false;
    NetworkedMultiplayerENet connection;

      Global global;

    public bool IsServer { get => isServer; set => isServer = value; }
    public NetworkedMultiplayerENet Connection { get => connection; set => connection = value; }

    public override void _Ready()
    {
        GD.Print("Miryp");
        global = (Global) GetNode("/root/Global");
        if(global.IsServer){
            initServer();
        }else{
            initClient();
        }
    }

    public void initServer()
    {
        IsServer = true;
        GD.Print("Start as sever");
        Connection = new NetworkedMultiplayerENet();
        Connection.CreateServer(16395, 3);
        GetTree().SetNetworkPeer(Connection);
        GetTree().Connect("network_peer_connected", this, nameof(OnClientConnected));


    }
    public void initClient(String address = "127.0.0.1")
    {
        GD.Print("Start as client");
        Connection = new NetworkedMultiplayerENet();
        Connection.CreateClient(address, 16395);
        GetTree().SetNetworkPeer(Connection);
    }

    public void OnClientConnected(int i)
    {
        GD.Print("OnClientConnected", i);
    }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
}
